from django.contrib.auth import views as auth_views
from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add/', add_friend, name='add_friend'),
    path('admin/login/', auth_views.LoginView.as_view()),
]
