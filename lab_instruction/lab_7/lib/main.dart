import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: SignUpForm(),
    //BelajarForm(),
  ));
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  String? _Username;
  String? _Email;
  String? _Password1;
  String? _Password2;
  String? _Saran;

  Widget _buildUsername() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Username',
      ),
      maxLength: 1024,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        _Username = value!;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      maxLength: 1024,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Pertanyaan ini wajib diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        _Email = value!;
      },
    );
  }

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  Widget _buildPassword1() {
    return Container(
      child: Column(
        children: [
          TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            maxLength: 1024,
            validator: (String? value) {
              if (value!.isEmpty) {
                return 'Pertanyaan ini wajib diisi';
              }

              return null;
            },
            obscureText: _obscureText,
            onSaved: (String? value) {
              _Password1 = value!;
            },
          ),
          TextButton(
              onPressed: _toggle,
              child: new Text(_obscureText ? "Show" : "Hide"))
        ],
      ),
    );
  }

  bool _obscureText2 = true;

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  Widget _buildPassword2() {
    return Container(
      child: Column(
        children: [
          TextFormField(
            decoration: InputDecoration(labelText: 'Password Confirmation'),
            maxLength: 1024,
            validator: (String? value) {
              if (value!.isEmpty) {
                return 'Pertanyaan ini wajib diisi';
              }

              return null;
            },
            obscureText: _obscureText2,
            onSaved: (String? value) {
              _Password2 = value!;
            },
          ),
          TextButton(
              onPressed: _toggle2,
              child: new Text(_obscureText2 ? "Show" : "Hide"))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Formulir Keluhan")),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildUsername(),
                const Text(
                    "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."),
                _buildEmail(),
                _buildPassword1(),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const <Widget>[
                      Text(
                          "• Your password can’t be too similar to your other personal information."),
                      Text(
                          "• Your password must contain at least 8 characters."),
                      Text(
                          "• Your password can’t be a commonly used password."),
                      Text("• Your password can’t be entirely numeric."),
                    ],
                  ),
                ),
                _buildPassword2(),
                Container(
                  child: Text(
                      "Enter the same password as before, for verification."),
                ),
                const SizedBox(height: 10),
                ElevatedButton(
                  child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }

                    _formKey.currentState!.save();

                    print(_Username);
                    print(_Email);
                    print(_Password1);
                    print(_Password2);

                    //Send to API
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text("Registrasi"),
  //     ),
  //     body: Form(
  //       key: _formKey,
  //       child: SingleChildScrollView(
  //         child: Container(
  //           padding: EdgeInsets.all(20.0),
  //           child: Column(
  //             children: [
  //               Padding(
  //                 padding: const EdgeInsets.all(8.0),
  //                 child: TextFormField(
  //                   decoration: new InputDecoration(
  //                     hintText: "contoh: Susilo Bambang",
  //                     labelText: "Nama Lengkap",
  //                     icon: Icon(Icons.people),
  //                     border: OutlineInputBorder(
  //                         borderRadius: new BorderRadius.circular(5.0)),
  //                   ),
  //                   validator: (value) {
  //                     if (value!.isEmpty) {
  //                       return 'Nama tidak boleh kosong';
  //                     }
  //                     return null;
  //                   },
  //                 ),
  //               ),
  //               Padding(
  //                 padding: const EdgeInsets.all(8.0),
  //                 child: TextFormField(
  //                   obscureText: true,
  //                   decoration: new InputDecoration(
  //                     labelText: "Password",
  //                     icon: Icon(Icons.security),
  //                     border: OutlineInputBorder(
  //                         borderRadius: new BorderRadius.circular(5.0)),
  //                   ),
  //                   validator: (value) {
  //                     if (value!.isEmpty) {
  //                       return 'Password tidak boleh kosong';
  //                     }
  //                     return null;
  //                   },
  //                 ),
  //               ),
  //               CheckboxListTile(
  //                 title: Text('Belajar Dasar Flutter'),
  //                 subtitle: Text('Dart, widget, http'),
  //                 value: nilaiCheckBox,
  //                 activeColor: Colors.deepPurpleAccent,
  //                 onChanged: (value) {
  //                   setState(() {
  //                     nilaiCheckBox = value!;
  //                   });
  //                 },
  //               ),
  //               SwitchListTile(
  //                 title: Text('Backend Programming'),
  //                 subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
  //                 value: nilaiSwitch,
  //                 activeTrackColor: Colors.pink[100],
  //                 activeColor: Colors.red,
  //                 onChanged: (value) {
  //                   setState(() {
  //                     nilaiSwitch = value;
  //                   });
  //                 },
  //               ),
  //               Slider(
  //                 value: nilaiSlider,
  //                 min: 0,
  //                 max: 100,
  //                 onChanged: (value) {
  //                   setState(() {
  //                     nilaiSlider = value;
  //                   });
  //                 },
  //               ),
  //               RaisedButton(
  //                 child: Text(
  //                   "Submit",
  //                   style: TextStyle(color: Colors.white),
  //                 ),
  //                 color: Colors.blue,
  //                 onPressed: () {
  //                   if (_formKey.currentState!.validate()) {}
  //                 },
  //               ),
  //             ],
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                  title: Text('Belajar Dasar Flutter'),
                  subtitle: Text('Dart, widget, http'),
                  value: nilaiCheckBox,
                  activeColor: Colors.deepPurpleAccent,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox = value!;
                    });
                  },
                ),
                SwitchListTile(
                  title: Text('Backend Programming'),
                  subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                  value: nilaiSwitch,
                  activeTrackColor: Colors.pink[100],
                  activeColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
