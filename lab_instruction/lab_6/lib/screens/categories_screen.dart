import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/category_item.dart';
import 'package:carousel_slider/carousel_slider.dart';

// class CategoriesScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       padding: const EdgeInsets.all(25),
//       children: DUMMY_CATEGORIES
//           .map(
//             (catData) => CategoryItem(
//               catData.id,
//               catData.title,
//               catData.color,
//             ),
//           )
//           .toList(),
//       // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
//       //   maxCrossAxisExtent: 200,
//       //   childAspectRatio: 3 / 2,
//       //   crossAxisSpacing: 20,
//       //   mainAxisSpacing: 20,
//       // ),
//     );
//   }
// }

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Column(
        children: <Widget>[
          CarouselSlider(
            options: CarouselOptions(height: 200),
            items: [
              Image.network(
                  'https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg',
                  height: 150,
                  width: 250),
              Image.network(
                  'https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg',
                  height: 50,
                  width: 250),
              Image(
                  image: NetworkImage(
                      'https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg')),
              Image(
                  image: NetworkImage(
                      'https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg')),
              Image(
                  image: NetworkImage(
                      'https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg')),
            ].map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0), child: i));
                },
              );
            }).toList(),
          ),
          Container(
              child: Text("SAFE FLIGHT",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 35,
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 2,
                    wordSpacing: 10,
                  ))),
          SizedBox(height: 10),
          Container(
              child: Text("stay healty, stay happy",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1,
                    wordSpacing: 5,
                  ))),
          SizedBox(height: 10),
          ElevatedButton(
            onPressed: () {},
            child: Text("Login Sekarang"),
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blue)),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
              child: Text("FITUR KAMI",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 3,
                    wordSpacing: 10,
                  ))),
          SizedBox(height: 10),
          Container(
            child: Column(
              children: [
                Center(
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Column(children: [
                          Text('Info Statistik',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                          SizedBox(height: 10),
                          Text(
                              'Kumpulan statistik mengenai jumlah kasus, vaksin, dan kematian akibat COVID-19 pada berbagai negara',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                        ]),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Column(children: [
                          Text('Regulasi',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                          SizedBox(height: 10),
                          Text(
                              'Kumpulan informasi mengenai kebijakan dan persyaratan untuk mengunjungi negara destinasi',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                        ]),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Column(children: [
                          Text('Get Swabbed!',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                          SizedBox(height: 10),
                          Text(
                              'Menyediakan daftar fasilitas tes covid-19 terdekat dari bandara utama di negara tujuan',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                        ]),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Column(children: [
                          Text('Info Hotel Karantina',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                          SizedBox(height: 10),
                          Text(
                              'Temukan hotel karantina terbaik di negara tujuanmu! Kenyamananmu adalah prioritas kami',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                        ]),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Column(children: [
                          Text('Artikel',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                          SizedBox(height: 10),
                          Text(
                              'Artikel dan penulisan artikel terkait traveling saat pandemi',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 1,
                                wordSpacing: 5,
                              )),
                        ]),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ]);
  }
}
