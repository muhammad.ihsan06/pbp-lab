# Lab 2: Data Delivery Using HTML, XML, and JSON

Nama    : Muhammad Ihsan Al Farisi

Kelas   : PBP-B

NPM     : 2006596693

---

## 1. Apakah perbedaan antara JSON dan XML?
Dalam sistem komputer terdapat berbagai macam data dan masing-masing data tersebut memiliki format data 
yang berbeda-beda. Di dunia web, XML dan JSON hadir sebagai format umum data yang dapat digunakan para 
_programmer._ XML atau _Extensible Markup Language_ adalah bahasa _markup_ yang berfungsi untuk mempermudah
pertukaran, pengiriman, dan penyimpanan antarserver. Sedangkan JSON atau _JavaScript Object Notation_ adalah
bahasa turunan JavaScript yang digunakan untuk penyimanan dan transfer data. XML dan JSON memang memiliki
kesamaan dari segini kegunaan, namun terdapat juga beberapa perbedaan antara lain:
- XML adalah bahasa _markup_, sedangkan JSON adalah bahasa _meta_
- Secara kompleksitas, XML lebih rumit dibandingkan dengan JSON di mana JSON lebih mudah untuk dibaca dan
dimengerti
- XML berorientasi pada dokumen, sedangkan JSON berorientasi pada data
- XML belum mendukung penggunaan _array_, sedangkan JSON sudah
- _File_ XML diakhiri dengan ekstensi .xml, sedangkan JSON dengan .json
- Perbedaan syntax XML dan JSON:
   
Contoh XML:
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<root>
    <siswa>
        <id>01</id>
        <nama>Tom</nama>
        <jurusan>IPA</jurusan>
    </siswa>
    <siswa>
        <id>02</id>
        <nama>Niki</nama>
        <jurusan>IPS</jurusan>
    </siswa>
</root>
```

Contoh JSON:
```json
{
  "siswa": [

    {
      "id":"01",
      "nama": "Tom",
      "jurusan": "IPA"
    },

    {
      "id":"02",
      "nama": "Niki",
      "jurusan": "IPS"
    }
  ]
}
```

## 2. Apakah perbedaan antara HTML dan XML?
HTML atau _Hypertext Markup Language_ adalah bahasa _markup_ yang digunakan para _web-developer_ untuk 
membuat struktur halaman _website_. Walau XML dan HTML sama-sama merupakan bahasa _markup_, keduanya 
memiliki perbedaan mendasar dari sisi fungsinya, XML berfungsi untuk menyimpan dan mentransfer data, 
sedangkan HTML digunakan untuk menampilkan data pada _website page_. Beberapa perbedaan lainnya di antaranya:
- Tipe bahasa XML adalah _case sensitive_, sedangkan HTML _case insensitive_
- Pada XML _programmer_ dapat mendefinisikan kumpulan _tag_-nya, sedangkan _tag_ pada HTML telah ditentukan
- _Tag_ penutup pada XML bersifat wajib, sedangkan pada HTML bersifat opsional

Contoh HTML:
```html
{<!DOCTYPE html>
<html>
<head>
  <title>Title of the document</title>
</head>
<body>
  <h1>Heading</h1>
  <p>Paragraf.</p>
</body>
</html>
```
